
var info = {
    name:       document.getElementById('userName'),
    login:      document.getElementById('userLogin'),
    email:      document.getElementById('email'),
    card:       document.getElementById('card'),
    time:       document.getElementById('time'),
    price:      document.getElementById('price'),


    elName:     document.getElementById('spanName'),
    elLogin:    document.getElementById('spanLogin'),
    elEmail:    document.getElementById('spanEmail'),
    elCard:     document.getElementById('spanCard'),
    elCard2:    document.getElementById('spanCard2'),
    elTime:     document.getElementById('spanTime'),
    elPrice:    document.getElementById('spanPrice'),


    but:        document.getElementById('sendData'),
    mass: [],

    validName: function(){
        var reg = /(^[A-Z]{1}[a-z]{1,14}$)/
             
        if (reg.test(this.name.value) === true || this.name.value === ''){
            this.elName.className = 'invisible'
            
        }

        else if(this.name.value.length >= 2){
            this.elName.className = 'visible'
            this.elName.className = 'text-muted'
        }
        
    },

    validLogin: function(){
        var reg = /(^[A-Z]{1}[a-z]{1,14}$)/       
        
        if (reg.test(this.login.value) === true || this.login.value === ''){
            this.elLogin.className = 'invisible'
        }

        else if(this.login.value.length > 3){
            this.elLogin.className = 'visible'
            this.elLogin.className = 'text-muted'
        }
        
    },

    validEmail: function(){
        var reg = /^(?!.*@.*@.*$)(?!.*@.*\-\-.*\..*$)(?!.*@.*\-\..*$)(?!.*@.*\-$)(.*@.+(\..{1,11})?)$/
                
        if (reg.test(this.email.value) === true || this.email.value === ''){
            this.elEmail.className = 'invisible'
        }

        else if(this.email.value.length > 3){
            this.elEmail.className = 'visible'
            this.elEmail.className = 'text-muted'
        }
        
    },

    validCard: function(){
        var reg1 = /^5[1-5][0-9]{14}$/ //mastercard
        var reg2 = /^4[0-9]{12}(?:[0-9]{3})?$/ //visa
        
        var first = this.card.value[0];
                      
        if(this.card.value.length > 3 && first === '4'){
                
            this.elCard2.className = 'visible'
            this.elCard2.className = 'text-muted'
        }

         if(this.card.value.length > 3 && first === '5'){
            this.elCard.className = 'visible'
            this.elCard.className = 'text-muted'
        }

        if(this.card.value.length > 3 && first != '5' && first != '4'){
            this.elCard2.className = 'visible'
            this.elCard2.className = 'text-muted'
            this.elCard.className = 'visible'
            this.elCard.className = 'text-muted'   
        }

        if (reg1.test(this.card.value) === true || reg2.test(this.card.value) === true || this.card.value === ''){
            this.elCard.className = 'invisible'
            this.elCard2.className = 'invisible'
        }
       
    },

    validTime: function(){
        var reg =  /^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/
                
        if (reg.test(this.time.value) === true || this.time.value === ''){
            this.elTime.className = 'invisible'
        }
        else if(this.time.value.length > 3){
            this.elTime.className = 'visible'
            this.elTime.className = 'text-muted'
        }
        
    },

    validPrice: function(){
        var reg =  /^(?!0.*$)([0-9]{1,3}(,[0-9]{3})?(,[0-9]{3})?(\.[0-9]{2})?)$/       
        
        if (reg.test(this.price.value) === true || this.price.value === ''){
            this.elPrice.className = 'invisible'
        }
        else if(this.price.value.length > 3){
            this.elPrice.className = 'visible'
            this.elPrice.className = 'text-muted'
        }
        
    },

    sendInfo: function(){

        if(this.elName.className === 'invisible' && this.elLogin.className === 'invisible' && this.elEmail.className === 'invisible' && 
        this.elCard.className === 'invisible' && this.elCard2.className === 'invisible' && this.elTime.className === 'invisible' && 
        this.elPrice.className === 'invisible' ){
            
            this.mass.push({
                name: this.name.value,
                login:this.login.value,
                email:this.email.value,
                card: this.card.value,
                time: this.time.value,
                price:this.price.value
            });

            this.showList();
        }

        else if(this.elName.className != 'invisible' || this.elLogin.className != 'invisible' || this.elEmail.className != 'invisible' || 
        this.elCard.className != 'invisible' || this.elCard2.className != 'invisible' || this.elTime.className != 'invisible' || 
        this.elPrice.className != 'invisible'){
            alert('you have a mistake, please check your data')
        }

    },

    showList: function()
    {
        var tab = document.getElementById('table1');
        var tbod = document.getElementById('tbody');
        
        for (var i in this.mass)
        {
            var trr = document.createElement('tr')
            var item = this.mass[i];

            for(var j in item)
            {
                var field = item[j]
                var td1 = document.createElement('td')
                td1.innerHTML = field;
                trr.appendChild(td1);

            }
        }

        tbod.appendChild(trr)
        tab.appendChild(tbod)
    },

    init: function(){
        this.but.onclick = this.sendInfo.bind(this)
        this.name.oninput = this.validName.bind(this)
        this.login.oninput = this.validLogin.bind(this)
        this.email.oninput = this.validEmail.bind(this)
        this.card.oninput = this.validCard.bind(this)
        this.time.oninput = this.validTime.bind(this)
        this.price.oninput = this.validPrice.bind(this)
    }
}.init();

